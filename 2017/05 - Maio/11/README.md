# Front Day - 11 de Maio de 2017

Todas as palestras, workshops e bate-papos realizados no dia 11 de maio de 2017.

## Palestras

* [Entendo os metodos e propriedades dos objetos em Javascript](Entendo os metodos e propriedades dos objetos em Javascript)
* [Performance com JavaScript](Performance com JavaScript)
* [React](React)
