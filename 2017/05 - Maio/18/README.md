# Front Day - 18 de Maio de 2017

Todas as palestras, workshops e bate-papos realizados no dia 18 de maio de 2017.

## Palestras

* (Git - Branches, Stash, Pull Requests, Tags e Commits)[Git - Branches, Stash, Pull Requests, Tags e Commits]
* (Sprites - Como utilizar e gerar imagens otimizadas)[Sprites - Como utilizar e gerar imagens otimizadas]
* (Web components o novo padrão da web)[Web components o novo padrão da web]
