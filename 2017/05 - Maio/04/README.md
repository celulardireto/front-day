# Front Day - 05 de Maio de 2017

Todas as palestras, workshops e bate-papos realizados no dia 05 de maio de 2017.

## Palestras

* [Padrões de Projetos](Padrões de Projetos)
* [Tipo primitivos e de referencia em Javascript](Tipo primitivos e de referencia em Javascript)
* [Três coisas que aprendi contribuindo para o Node Core](Três coisas que aprendi contribuindo para o Node Core)
